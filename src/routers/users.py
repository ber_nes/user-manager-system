from sqlmodel import Session
from fastapi import APIRouter, Depends, Query, status

from src.dependencies import db
from src.db import get_from_db, get_all_from_db, save_to_db
from src.models import users
from src.schemas.users import User, UserCreate, UserUpdate
from src.utils import update_entity
from src.validations.users import email_is_unique


router = APIRouter(prefix="/users", tags=["users"])


@router.post("/", response_model=User)

def create_user(user: UserCreate, session: Session = Depends(db.get_session)):
    email_is_unique(session, user.email)
    db_user = users.User.from_orm(user)
    return save_to_db(session, db_user)


@router.get("/", response_model=list[User])
def get_users(session: Session = Depends(db.get_session),
              offset: int = 0,
              limit: int = Query(default=5, lte=100)):
    return get_all_from_db(session, users.User, offset, limit)


@router.get("/{user_id}", response_model=User)
def get_user(user_id: int, session: Session = Depends(db.get_session)):
    return get_from_db(session, users.User, user_id)


@router.put("/{user_id}", response_model=User)
def update_user(user_id: int, 
                user: UserUpdate, 
                session: Session = Depends(db.get_session)):
    if user.email:
        email_is_unique(session, user.email)
    user_db = get_from_db(session, users.User, user_id)
    user_updated = update_entity(user, user_db)
    return save_to_db(session, user_updated)


@router.delete("/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_user(user_id: int, session: Session = Depends(db.get_session)):
    user_db = get_from_db(session, users.User, user_id)
    session.delete(user_db)
    session.commit()
    return