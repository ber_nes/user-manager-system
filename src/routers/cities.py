from sqlmodel import Session
from fastapi import APIRouter, Depends, Query, status

from src.dependencies import db
from src.db import get_from_db, get_all_from_db, save_to_db
from src.models import cities
from src.schemas.cities import City, CityCreate, CityUpdate
from src.utils import update_entity
from src.validations.cities import cityname_is_unique


router = APIRouter(prefix="/cities", tags=["cities"])


@router.post("/", response_model=City)
def create_city(city: CityCreate, session: Session = Depends(db.get_session)):
    cityname_is_unique(session, city.name)
    db_city = cities.City.from_orm(city)
    return save_to_db(session, db_city)


@router.get("/", response_model=list[City])
def get_cities(session: Session = Depends(db.get_session),
               offset: int = 0,
               limit: int = Query(default=5, lte=100)):
    return get_all_from_db(session, cities.City, offset, limit)


@router.get("/{city_id}", response_model=City)
def get_city(city_id: int, session: Session = Depends(db.get_session)):
    return get_from_db(session, cities.City, city_id)


@router.put("/{city_id}", response_model=City)
def update_city(city_id: int, 
                city: CityUpdate, 
                session: Session = Depends(db.get_session)):
    if city.name:
        cityname_is_unique(session, city.name)
    city_db = get_from_db(session, cities.City, city_id)
    city_updated = update_entity(city, city_db)
    return save_to_db(session, city_updated)


@router.delete("/{city_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_city(city_id: int, session: Session = Depends(db.get_session)):
    city_db = get_from_db(session, cities.City, city_id)
    session.delete(city_db)
    session.commit()
    return
