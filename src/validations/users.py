from fastapi import HTTPException, status
from sqlmodel import select, Session

from src.models.users import User


def email_is_unique(session: Session, email: str):
    if session.exec(select(User).where(User.email == email)).first():
        raise HTTPException(status.HTTP_400_BAD_REQUEST,
                            "Email already exist")
