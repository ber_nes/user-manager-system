from fastapi import HTTPException, status
from sqlmodel import select, Session

from src.models.cities import City


def cityname_is_unique(session: Session, name: str):
    if session.exec(select(City).where(City.name == name)).first():
        raise HTTPException(status.HTTP_400_BAD_REQUEST,
                            "City name already exist")
