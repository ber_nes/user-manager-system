from sqlmodel import SQLModel


def update_entity(updated_entity: SQLModel, original_entity: SQLModel):
    entity_data_update = updated_entity.dict(exclude_unset=True)
    for k, v in entity_data_update.items():
        setattr(original_entity, k, v)
    return original_entity
