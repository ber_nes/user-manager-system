from typing import Optional

from sqlmodel import Field, Relationship, SQLModel

from src.models import cities, users


class Client(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str = Field(max_length=150)
    city_id: int = Field(foreign_key="city.id")
    user_id: int = Field(foreign_key="user.id")
    city: cities.City = Relationship()
    user: users.User = Relationship(back_populates="clients")
