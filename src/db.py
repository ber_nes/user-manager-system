from fastapi import HTTPException
from sqlmodel import create_engine, select, Session, SQLModel

from src.models import (cities, clients, users)
from src.settings import URI


engine = create_engine(URI, echo=True)


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


def save_to_db(session: Session, model: SQLModel):
    session.add(model)
    session.commit()
    session.refresh(model)
    return model


def get_all_from_db(session: Session, model: SQLModel, offset: int, limit: int):
    return session.exec(select(model).offset(offset).limit(limit)).all()


def get_from_db(session: Session, entity: SQLModel, entity_id: int) -> SQLModel:
    entity_db = session.get(entity, entity_id)
    if not entity_db:
        raise HTTPException(status_code=404,
                            detail=f"{entity.__name__} not found")
    return entity_db
