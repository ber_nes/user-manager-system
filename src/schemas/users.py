from typing import Optional, TYPE_CHECKING

from pydantic import BaseModel
    

class UserBase(BaseModel):
    name: str
    password: str
    email: str
    photo: Optional[str]


class UserCreate(UserBase):
    pass


class User(UserBase):
    id: int

    class Config:
        orm_mode = True


class UserUpdate(BaseModel):
    name: Optional[str]
    password: Optional[str]
    email: Optional[str]
    photo: Optional[str]
