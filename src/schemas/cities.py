from typing import Optional

from pydantic import BaseModel


class CityBase(BaseModel):
    name: str


class CityCreate(CityBase):
    pass


class City(CityBase):
    id: int
    
    class Config:
        orm_mode = True


class CityUpdate(BaseModel):
    name: Optional[str]

