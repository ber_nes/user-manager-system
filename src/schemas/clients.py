from typing import Optional

from pydantic import BaseModel


class ClientBase(BaseModel):
    name: str
    city_id: int
    user_id: int


class ClientCreate(ClientBase):
    pass


class Client(ClientBase):
    id: int

    class Config:
        orm_mode = True


class ClientUpdate(BaseModel):
    name: Optional[str]
    city_id: Optional[int]
