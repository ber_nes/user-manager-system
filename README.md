# Project Name

## Features

- Establishment of database schema and predefined data.
- CRUD functionality for each entity (client, user, city).
- Creation of templates for data visualization and registration.
- Implementation of pagination to handle large volumes of data.
- Addition of a filter to search clients by city.
- Ability to export client data to Excel format.
- Implementation of user sessions and login functionality.
- Generation of a JWT-authenticated REST API to access the data.

## Description

This project involves the development of a client management application based on cities. The application utilizes a database with three tables: `client`, `users`, and `cities`. The following are the main features of the application:

1. **Establishment of database schema and predefined data**: Defines the structure of the database and provides a predefined set of initial data.

2. **CRUD functionality for each entity**: Enables creating, reading, updating, and deleting records for the `client`, `user`, and `city` entities.

3. **Creation of templates for data visualization and registration**: Implements templates that allow intuitive and efficient display and registration of information about clients, users, and cities.

4. **Implementation of pagination**: Includes a pagination mechanism to handle large volumes of data, facilitating navigation and optimizing application performance.

5. **Filter clients by city**: Adds the ability to filter and display only clients associated with a specific city, enhancing the user experience when working with specific data.

6. **Export client data to Excel format**: Provides a function to export client data to an Excel file, facilitating analysis and further use.

7. **User sessions and login functionality**: Implements a user session system that requires authentication to access application functionalities, ensuring security and access control.

8. **Generation of a JWT-authenticated REST API**: Creates a REST API that allows interaction with the application data, using JWT authentication to ensure the security of requests and protect data integrity.

## Requirements

- Python 3.x
- Additional libraries: [list of required libraries]
- Database: [name of the database management system used]

## Installation

1. Clone this repository.
2. [Additional installation steps if any]

## Usage

1. Configure the database and predefined data.
2. Start the application by running the [name of the main file].
3. Access the application through [URL or additional instructions].

## Contribution

If you would like to contribute to this project, follow these steps:

1. Fork this repository.
2. Create a branch for your new feature (`git checkout -b feature/new-feature`).
3. Make changes and commit them (`git commit -m 'Add new feature'`).
4. Push the changes to your forked repository (`git push origin feature/new-feature`).
5. Open a pull request with a detailed description of the feature.
